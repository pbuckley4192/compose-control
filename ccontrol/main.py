
from cement import App, TestApp, init_defaults
from cement.core.exc import CaughtSignal
from .core.exc import ComposeControError
from .controllers.base import Base
from .controllers.configure import Configure
from .controllers.logs import Logs
from .controllers.start import Start
from .controllers.interactive import Interactive

# configuration defaults
CONFIG = init_defaults('ccontrol_config')
CONFIG['ccontrol_config']['compose_file'] = ''

def log_config(app):
    '''app.log.info('showing conf params')
    app.log.info('compose_file is {0}'.format(app.config.get('ccontrol_config', 'compose_file')))'''

class ComposeContro(App):
    """Compose Control primary application."""

    class Meta:
        label = 'ccontrol'

        # configuration defaults
        config_defaults = CONFIG

        # call sys.exit() on close
        close_on_exit = True

        # load additional framework extensions
        extensions = [
            'yaml',
            'colorlog',
            'jinja2',
        ]

        # configuration handler
        config_handler = 'yaml'

        config_files=[
            './config/ccontrol.yml',
            '~/.ccontrol/config.yml',
        ]

        # configuration file suffix
        config_file_suffix = '.yml'

        # set the log handler
        log_handler = 'colorlog'

        # set the output handler
        output_handler = 'jinja2'

        # register handlers
        handlers = [
            Base,
            Configure,
            Logs,
            Start,
            Interactive
        ]

        # Add hooks
        hooks = [
            ('post_setup', log_config),
        ]


class ComposeControTest(TestApp,ComposeContro):
    """A sub-class of ComposeContro that is better suited for testing."""

    class Meta:
        label = 'ccontrol'


def main():
    with ComposeContro() as app:
        try:
            app.run()

        except AssertionError as e:
            print('AssertionError > %s' % e.args[0])
            app.exit_code = 1

            if app.debug is True:
                import traceback
                traceback.print_exc()

        except ComposeControError:
            print('ComposeControError > %s' % e.args[0])
            app.exit_code = 1

            if app.debug is True:
                import traceback
                traceback.print_exc()

        except CaughtSignal as e:
            # Default Cement signals are SIGINT and SIGTERM, exit 0 (non-error)
            print('\n%s' % e)
            app.exit_code = 0


if __name__ == '__main__':
    main()
