import os, yaml, re
from cement import Controller, ex
from cement.utils import fs

class Configure(Controller):

    CONF_FILE = fs.abspath('~/.ccontrol/config.yml')
    CONF_DIR = os.path.dirname(CONF_FILE)
    SAVE_PREFIX = "save."

    class Meta:
        label = 'configure'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(
        help='configure the compose system to use',

        # sub-command level arguments. ex: 'cloudplatform configure --foo bar'
        arguments=[
            ( [ '-f', '--file' ],
              { 'help' : 'set the docker compose file which will be placed under control',
                'action'  : 'store',
                'dest' : 'compose_file' } ),
            ( [ '-x', '--exclude-service' ],
              { 'help' : 'mark a service matching this name as to not be shown in the logs',
                'action'  : 'store',
                'dest' : 'exclude_service' } ),
            ( [ '-i', '--include-service' ],
              { 'help' : 'remove a service from the logging exclusion functionality',
                'action'  : 'store',
                'dest' : 'include_service' } ),
            ( [ '-s', '--save-configuration' ],
              { 'help' : 'save the current configuration',
                'action'  : 'store',
                'dest' : 'save_configuration' } ),
            ( [ '-l', '--load-configuration' ],
              { 'help' : 'load a saved configuration',
                'action'  : 'store',
                'dest' : 'load_configuration' } ),
            ( [ '-v', '--view-configurations' ],
              { 'help' : 'view a list of all of the saved configurations',
                'action'  : 'store_true',
                'dest' : 'view_configurations' } ),
        ],
    )

    def configure(self):
        """Configure the compose file to place under control"""
        # check if we already have a configuration file, make a default file
        # define config file
        self.initConfigFile()
        # Check for arguments
        show_config = True
        # Load the configuration dictionary
        conf_dict = self.loadConfigFile()
        # check if we have any params, if not configure the whole app
        # If we are passed a file, store it in the config
        if self.app.pargs.compose_file:
            # write the new configuration out to the file
            conf_dict['ccontrol_config']['compose_file'] = self.app.pargs.compose_file
            self.saveConfigFile(conf_dict)

        # if we are passed a sevice to exclude from the logs, store it in the
        # configuration
        if self.app.pargs.exclude_service:
            self.excludeService(conf_dict)

        # If a param is passed to include a file back into the monitoring, remove it
        # from the excluded services in the configuration file
        if self.app.pargs.include_service:
            self.includeService(conf_dict)

        # Save the configuration for reuse later
        if self.app.pargs.save_configuration:
            self.saveConfiguration(conf_dict, self.app.pargs.save_configuration)

        # Load in a saved configuration
        if self.app.pargs.load_configuration:
            conf_dict =  self.loadConfiguration(self.app.pargs.load_configuration)

        # show the saved configurations
        if self.app.pargs.view_configurations:
            found_saves = {'saves':[]}
            for file in os.listdir(self.CONF_DIR):
                if file.startswith("save."):
                    m = re.search(r'save.([^<]*).yml', file)
                    found_saves['saves'].append(m.group(1))
            if len(found_saves['saves']) > 0:
                self.app.render(found_saves, 'saved_configurations.jinja2')
                show_config = False
            else:
                print("\nNo saved configurations have been found.\n")

        # If we have actioned  a command, then display the config file
        if show_config is True:
            # Show the current config
            self.app.render(conf_dict, 'configuration.jinja2')
        else:
            # default function if no param is passed
            if self.app.pargs.view_configurations is None and self.app.pargs.compose_file is None:
                # check if the conf_dict has a value set
              if conf_dict['ccontrol_config']['compose_file'] == 'new-file':
                  self.app.render(conf_dict, 'configuration_notset.jinja2')
              else:
                  self.app.render(conf_dict, 'configuration.jinja2')


    def saveConfiguration(self, conf_dict, save_name):
        file = fs.abspath('~/.ccontrol/' + self.SAVE_PREFIX + save_name + '.yml')
        f= open(file, 'w+')
        yaml.dump(conf_dict, f, default_flow_style=False)
        f.close()
        print("Saved configuratiom : '{0}'".format(save_name))

    def loadConfiguration(self, saved_configuration_name):
        dict = yaml.load(open(fs.abspath('~/.ccontrol/' + self.SAVE_PREFIX + saved_configuration_name + '.yml')))
        self.saveConfigFile(dict)
        print("Loaded configuration : '{0}'".format(saved_configuration_name))
        return dict

    def includeService(self, conf_dict):
        if 'excluded_services' in conf_dict['ccontrol_config']:
            if self.app.pargs.include_service in conf_dict['ccontrol_config']['excluded_services']:
                conf_dict['ccontrol_config']['excluded_services'].remove(self.app.pargs.include_service)
                self.saveConfigFile(conf_dict)

    def excludeService(self, conf_dict):
        # Check to see if we have created an exclude dictionary entry
        if 'excluded_services' in conf_dict['ccontrol_config']:
            # if the excluded_services does not contain the entry, add it
            if self.app.pargs.exclude_service in conf_dict['ccontrol_config']['excluded_services']:
                print("'{0}' is already an excluded service from logs:".format(self.app.pargs.exclude_service))
            else:
                conf_dict['ccontrol_config']['excluded_services'].append(self.app.pargs.exclude_service)
                self.saveConfigFile(conf_dict)
                print("Added '{0}' as an excluded service from logs:".format(self.app.pargs.exclude_service))
        else:
            conf_dict['ccontrol_config']['excluded_services'] = [self.app.pargs.exclude_service]
            self.saveConfigFile(conf_dict)
            print("Added '{0}' as an excluded service from logs:".format(self.app.pargs.exclude_service))

    def saveConfigFile(self, dictionary):
        f= open(self.CONF_FILE, 'w+')
        yaml.dump(dictionary, f, default_flow_style=False)
        f.close()

    def loadConfigFile(self):
        return yaml.load(open(self.CONF_FILE))

    def initConfigFile(self):
        # make the config directory if we need to
        if not os.path.exists(self.CONF_DIR):
            os.makedirs(self.CONF_DIR)
        if os.path.isfile(self.CONF_FILE) is False:
            # write an initial entry
            data = {'compose_file' : 'new-file'}
            f= open(self.CONF_FILE, 'w+')
            self.app.render(data, 'config.jinja2', f)
            f.close()
