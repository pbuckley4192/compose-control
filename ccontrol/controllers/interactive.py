from __future__ import unicode_literals, print_function
from prompt_toolkit import print_formatted_text, HTML
from prompt_toolkit.styles import Style
from prompt_toolkit import prompt
from prompt_toolkit.application import run_in_terminal
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit import Application
from prompt_toolkit.document import Document
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.filters import Condition
from prompt_toolkit.application import get_app
from prompt_toolkit.layout.containers import HSplit, VSplit, Window, ConditionalContainer, WindowAlign
from prompt_toolkit.layout.controls import BufferControl, FormattedTextControl
from prompt_toolkit.widgets import VerticalLine, Box, Frame, RadioList, Button, TextArea, SystemToolbar
from prompt_toolkit.layout.layout import Layout
import os, yaml, json
from cement import Controller, ex
from prompt_toolkit import prompt
from cement.utils import fs
from prettytable import PrettyTable
import subprocess

class Interactive(Controller):

    CONF_FILE = fs.abspath('~/.ccontrol/config.yml')

    class Meta:
        label = 'control'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(
        help='interactive control of the docker compose file',

        # sub-command level arguments. ex: 'cloudplatform configure --foo bar'
        arguments=[],
    )

    def interactive(self):
        """Interactive control of the docker-compose file"""
        # run the docker-compose file
        compose_file = self.app.config.get('ccontrol_config', 'compose_file')
        # used to determine if we need to reopen a screen
        config_file = yaml.load(open(self.CONF_FILE))

        style = Style.from_dict({
            'status': 'reverse'
        })

        @Condition
        def show_input_bar():
            return self.display_input
        
        @Condition
        def show_status_bar():
            if self.display_input is False:
                return True
            else:
                return False

        bindings = KeyBindings()
        @bindings.add('c-x')
        def _(event):
            " Exit when `c-x` is pressed. "
            def print_exit():
                print("Please wait... stopping docker-compose services.")
                subprocess.call(["docker-compose", "-f", compose_file, "down"])
            run_in_terminal(print_exit)
            event.app.exit()

        @bindings.add('c-r')
        def _(event):
            " Restart a service when `c-r` is pressed. "
            if self.display_input is True:
                self.display_input = False
            else:
                self.display_input = True 
                get_app().layout.focus(status_input_field)

        def restart_service(buff):
            self.display_input = False
            # Attempt to restart the service
            # Need to add a threaded refresh of the UI to show that the service is restarting
            output_text = ''
            if buff.text in self.compose_service_dict.keys():
                FNULL = open(os.devnull, 'w')
                subprocess.call(['docker-compose', '-f', compose_file, 'restart', self.compose_service_dict[buff.text]['service']], stdout=FNULL, stderr=subprocess.STDOUT)
                FNULL.close()
                output_text += "Restarted service '{0}'".format(self.compose_service_dict[buff.text]['service'])
                # check if we have a service we should restart in screen
                # services are excluded by service name check if its contained in the config
                if self.compose_service_dict[buff.text]['container'] not in config_file['ccontrol_config']['excluded_services']:
                   subprocess.call(['screen', '-t', self.compose_service_dict[buff.text]['container'], 'docker', 'logs', '--follow', self.compose_service_dict[buff.text]['container']]) 
                   # I need someway to refocus the previously selected window 

            else:
                output_text = output_text + "\n'{0}' does not seem to be a service index, please select a valid index".format(buff.text)

            self.output_field.buffer.document = Document(
                text=output_text, cursor_position=len(output_text))


        status_input_field = TextArea(text=u'', multiline=False, focusable=True, style='class:status', accept_handler=restart_service)

        root_container = HSplit([
            HSplit([
                VSplit([
                    Window(FormattedTextControl("Ctrl 0"), style='class:status', width=6, align=WindowAlign.LEFT),
                    Window(FormattedTextControl("Docker Processes"), style='class:status', align=WindowAlign.CENTER),
                ], height=1),
                Window(FormattedTextControl(self.get_docker_ps_list), align=WindowAlign.LEFT),
                VSplit([
                    Box(HSplit([
                        VSplit([
                            Window(FormattedTextControl("Ctrl 1"), style='class:status', width=6, align=WindowAlign.LEFT),
                            Window(FormattedTextControl("Service In Compose File"), style='class:status', align=WindowAlign.CENTER),
                        ], height=1),
                        Window(content=FormattedTextControl(text=self.get_compose_services())),
                    ]),padding=1),
                    Box(HSplit([
                        VSplit([
                            Window(FormattedTextControl("Ctrl 2"), style='class:status', width=6, align=WindowAlign.LEFT),
                            Window(FormattedTextControl("Services Log Status"), style='class:status', height=1, align=WindowAlign.CENTER),
                        ], height=1),
                        self.output_field
                    ]), padding=1),
                    Box(HSplit([
                        VSplit([
                            Window(FormattedTextControl("Ctrl 3"), style='class:status', width=6, align=WindowAlign.LEFT),
                            Window(FormattedTextControl("Kubernetes Pods"), style='class:status', height=1, align=WindowAlign.CENTER),
                        ], height=1),
                        Window(content=FormattedTextControl(text=self.k8s_get_pods())),
                    ]), padding=1),
                ])
            ]),
            # show the command status bar
            ConditionalContainer(
                VSplit([
                    Window(FormattedTextControl(self.select_index), style='class:status', height=1, width=15, align=WindowAlign.LEFT), 
                    status_input_field
                ]), 
                show_input_bar),
            ConditionalContainer(Window(FormattedTextControl(self.bottom_toolbar), style='class:status', height=1, align=WindowAlign.CENTER), show_status_bar)
        ])

        layout = Layout(root_container)

        guiapp = Application(layout=layout, full_screen=True, key_bindings=bindings, style=style)
        guiapp.run()

    '''
    This is used to store the output in the logging list area.. name to change
    '''    
    output_field = TextArea(text='This will be the logging list') 

    '''
    This flag holds whether or not we should show the input field
    '''
    display_input=False

    '''
    This holds a list of all the services and the index for that service in the docker compose list
    '''
    compose_service_dict = {}

    def bottom_toolbar(self):
        return HTML('<b>Ctrl x</b> - terminate | <b>Ctrl r</b> - restart service')

    def select_index(self):
        return HTML('<b>Select Index</b>:')

    def get_docker_ps_list(self):
        return subprocess.check_output(["docker", "ps"]).decode('utf-8', 'ignore')

    def get_compose_services(self):
        self.compose_service_dict = {}
        compose_dict = yaml.load(open(fs.abspath(self.app.config.get('ccontrol_config', 'compose_file'))))
        count = 0
        t = PrettyTable(['Index', 'Service', 'Container Name', 'Image'])
        if compose_dict['services'] is not None:
            for service in compose_dict['services']:
                container = ""
                if 'container_name' in compose_dict['services'][service] and compose_dict['services'][service]['container_name'] is not None:
                    container = compose_dict['services'][service]['container_name']
                t.add_row([count, service, container, compose_dict['services'][service]['image']])
                # set the key as a string to make it easier to search
                container_name = ""
                if 'conatiner_name' in compose_dict['services'][service]:
                    container_name = compose_dict['services'][service]['container_name']
                self.compose_service_dict[str(count)] = {'service' : service, 'container' : container_name}
                count = count+1
        return t.get_string

    def k8s_get_pods(self):
        return subprocess.check_output(["kubectl", "get","pods"]).decode('utf-8', 'ignore')

    



