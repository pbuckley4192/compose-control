
from setuptools import setup, find_packages
from ccontrol.core.version import get_version

VERSION = get_version()

f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name='ccontrol',
    version=VERSION,
    description='Control for Docker Compose',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Martin McCarry',
    author_email='john.doe@example.com',
    url='https://gitlab.com/mmccarry/compose-control',
    license='GPL v3.0',
    packages=find_packages(exclude=['ez_setup', 'tests*']),
    package_data={'ccontrol': ['templates/*']},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        ccontrol = ccontrol.main:main
    """,
)
