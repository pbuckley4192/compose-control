# Compose Control

A log monitor or and control system for watching docker-compose files.

THIS PROJECT IS WIP!!

## Prerequisites

You will need the following to run this utility.

```
Docker
Docker Compose
Python3
Pip3
Linux/Mac type OS (Tested on Mac OS and Ubuntu)
```
## Getting Started

Download the latest version of the source repository and ensure you are running python3.  
In the repository directory:

```
make virtualenv
source .env/bin/activate
```
This will set up a virtual env with the ccontrol utility installed.

## Descripion

### Examples
```
$ ccontrol --help
$ ccontrol configure -f /path/to/docker-compose.yaml
$ ccontrol configure
$ ccontrol logs
```

## Built With

## Contributing

## Versioning

## Authors

## License

This project is licensed under the GPL v3.0 License

## Acknowledgments
